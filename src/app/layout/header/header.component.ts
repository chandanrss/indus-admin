import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  currentDate: any;
  now: number | undefined;

  constructor(private router: Router) {
    setInterval(() => {
      this.now = Date.now();
    }, 1);
  }

  ngOnInit() {
  }

}
