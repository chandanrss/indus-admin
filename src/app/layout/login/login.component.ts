import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string | null = "indus@admin.com";
  password: string | null = "indus123";

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  login(): void {
    if (this.email && this.password) {
      if (this.email == 'indus@admin.com' && this.password == 'indus123') this.router.navigateByUrl('/home/dash');
      else if (this.email == 'partner@vendor.com' && this.password == 'partner123') {
        this.router.navigateByUrl('/home/dash');
      }
    } else
      this.router.navigateByUrl('/');
  }

}
