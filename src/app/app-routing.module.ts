import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './layout/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';
import { IndusDashComponent } from './pages/indus-dash/indus-dash.component';
import { VendorDashComponent } from './pages/vendor-dash/vendor-dash.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },

  {
    path: 'home',
    component: HomeComponent,
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
