import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LayoutModule } from '../layout/layout.module';
import { RouterModule } from '@angular/router';
import { PagesRoutingModule } from './pages-routing.module';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { IndusDashComponent } from './indus-dash/indus-dash.component';
import { VendorDashComponent } from './vendor-dash/vendor-dash.component';
import { AddRecoComponent } from './add-reco/add-reco.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AddBuiltComponent } from './add-built/add-built.component';
import { SitesComponent } from './sites/sites.component';
import { SitesPermissionComponent } from './sites-permission/sites-permission.component';
import { SitesPayForcastComponent } from './sites-pay-forcast/sites-pay-forcast.component';
import { RecoNorthMcdComponent } from './reco-north-mcd/reco-north-mcd.component';
import { RecoSouthMcdComponent } from './reco-south-mcd/reco-south-mcd.component';
import { RecoEastMcdComponent } from './reco-east-mcd/reco-east-mcd.component';
import { RecoDashComponent } from './reco-dash/reco-dash.component';
import { NewBuildDashComponent } from './new-build-dash/new-build-dash.component';
import { RecoListComponent } from './reco-list/reco-list.component';
import { BuiltListComponent } from './built-list/built-list.component';



@NgModule({
  declarations: [
    HomeComponent,
    DashboardComponent,
    IndusDashComponent,
    VendorDashComponent,
    AddRecoComponent,
    AddBuiltComponent,
    SitesComponent,
    SitesPermissionComponent,
    SitesPayForcastComponent,
    RecoNorthMcdComponent,
    RecoSouthMcdComponent,
    RecoEastMcdComponent,
    RecoDashComponent,
    NewBuildDashComponent,
    RecoListComponent,
    BuiltListComponent,
  ],
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule,
    PagesRoutingModule,
    DataTablesModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxChartsModule
  ],
  exports: [
    // HomeComponent
  ]
})
export class PagesModule { }
