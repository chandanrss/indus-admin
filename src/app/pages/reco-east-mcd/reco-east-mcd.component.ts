import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reco-east-mcd',
  templateUrl: './reco-east-mcd.component.html',
  styleUrls: ['./reco-east-mcd.component.css']
})
export class RecoEastMcdComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  view: any[] = [700, 400];

  // options
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = true;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Region';
  showYAxisLabel: boolean = true;
  yAxisLabel: string = 'No of Sites';
  legendTitle: string = 'Param';

  colorScheme: any = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA', '#CB3B1D', '#23D8E3', '#BA23E3']
  };

  onSelect(data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  multi: any = [
    {
      name: 'Shahdara North',
      series: [
        {
          name: 'Total Sites',
          value: 288
        },
        {
          name: 'Dismantled',
          value: 12
        },
        {
          name: 'Old Permission',
          value: 194
        },
        {
          name: 'Fresh Permissions',
          value: 22
        },
        {
          name: 'Old & Fresh Permission',
          value: 1
        },
        {
          name: 'Permission Denied',
          value: 0
        },
        {
          name: 'Total Permission Pending',
          value: 59
        },
        {
          name: 'Pole Sites Before 2016',
          value: 0
        },
        {
          name: 'Net Permssion Pending',
          value: 59
        }
      ]
    },
    {
      name: 'Shahdara South',
      series: [
        {
          name: 'Total Sites',
          value: 491
        },
        {
          name: 'Dismantled',
          value: 19
        },
        {
          name: 'Old Permission',
          value: 385
        },
        {
          name: 'Fresh Permissions',
          value: 76
        },
        {
          name: 'Old & Fresh Permission',
          value: 1
        },
        {
          name: 'Permission Denied',
          value: 0
        },
        {
          name: 'Total Permission Pending',
          value: 10
        },
        {
          name: 'Pole Sites Before 2016',
          value: 0
        },
        {
          name: 'Net Permssion Pending',
          value: 10
        }
      ]
    }

  ];


  //Grahp 3 and 4

  overViewData: any[] = [
    {
      "name": "City SP",
      "value": 8070000
    },
    {
      "name": "Civil Lines",
      "value": 18290000
    },
    {
      "name": "Karol Bagh",
      "value": 16640000
    },
    {
      "name": "Keshavpuram",
      "value": 21610000
    },
    {
      "name": "Narela",
      "value": 5050000
    },
    {
      "name": "Rohini",
      "value": 16630000
    }
  ];


  // options
  // showXAxis = true;
  // showYAxis = true;
  // gradient = false;
  showLegend2 = false;
  // showXAxisLabel = true;
  xAxisLabel1 = 'Cities/Area';
  // showYAxisLabel = true;
  yAxisLabel1 = 'Fund';

  colorScheme2: any = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };


  financialData: any[] = [
    {
      "name": "Amount FD",
      "value": 39625000
    },
    {
      "name": "Amount DD",
      "value": 39625000
    },
    {
      "name": "Amount Agt. Intrest",
      "value": 31468486
    },
    {
      "name": "Aount Utilized By Zone",
      "value": 7890000
    },
    {
      "name": "Balance Available",
      "value": 102828486
    }
  ];

  xAxisLabel2 = 'Medium';
  yAxisLabel2 = 'Amount';



}
