import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoEastMcdComponent } from './reco-east-mcd.component';

describe('RecoEastMcdComponent', () => {
  let component: RecoEastMcdComponent;
  let fixture: ComponentFixture<RecoEastMcdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecoEastMcdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoEastMcdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
