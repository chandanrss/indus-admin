import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SitesPermissionComponent } from './sites-permission.component';

describe('SitesPermissionComponent', () => {
  let component: SitesPermissionComponent;
  let fixture: ComponentFixture<SitesPermissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SitesPermissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SitesPermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
