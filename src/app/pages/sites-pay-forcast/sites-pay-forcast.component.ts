import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor } from '@angular/common/http';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-sites-pay-forcast',
  templateUrl: './sites-pay-forcast.component.html',
  styleUrls: ['./sites-pay-forcast.component.css']
})
export class SitesPayForcastComponent implements OnInit {

  constructor(private httpClient: HttpClient, private spinner: NgxSpinnerService) { }
  siteList: any;
  dtOptions: DataTables.Settings = {};

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();

  ngOnInit(): void {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 15
    };
    this.getSiteList();

    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
  }

  getSiteList() {
    this.spinner.show();
    let route = 'site';

    this.httpClient.get<{}>("http://localhost:3000/api" + '/' + route)
      .subscribe((res: any) => {
        this.spinner.hide();
        console.log(res);
        this.siteList = (res as any);
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      });
  }

}
