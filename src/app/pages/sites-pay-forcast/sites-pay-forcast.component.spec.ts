import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SitesPayForcastComponent } from './sites-pay-forcast.component';

describe('SitesPayForcastComponent', () => {
  let component: SitesPayForcastComponent;
  let fixture: ComponentFixture<SitesPayForcastComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SitesPayForcastComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SitesPayForcastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
