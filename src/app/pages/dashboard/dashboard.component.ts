import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  closeModal!: string;

  constructor(private modalService: NgbModal) { }
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = true;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Region';
  showYAxisLabel: boolean = true;
  yAxisLabel: string = 'No of Sites';
  legendTitle: string = 'Param';

  colorScheme: any = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA', '#CB3B1D', '#23D8E3', '#BA23E3']
  };

  ngOnInit(): void {
  }




  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }


  multi: any = [
    {
      name: 'North MCD',
      series: [
        {
          name: 'GBM',
          value: 1080
        },
        {
          name: 'RTP',
          value: 800
        },
        {
          name: 'COW',
          value: 90
        }

      ]
    },
    {
      name: 'SOUTH MCD',
      series: [
        {
          name: 'GBM',
          value: 1250
        },
        {
          name: 'RTP',
          value: 950
        },
        {
          name: 'COW',
          value: 120
        }

      ]
    },
    {
      name: 'EAST MCD',
      series: [
        {
          name: 'GBM',
          value: 500
        },
        {
          name: 'RTP',
          value: 400
        },
        {
          name: 'COW',
          value: 10
        }

      ]
    },


  ];

  colorScheme2: any = {
    domain: ['#0000FF', '#FF0000', '#00FF00']
  };

  multi2: any = [
    {
      name: 'GBM',
      series: [
        {
          name: 'North MCD',
          value: 32
        },
        {
          name: 'South MCD',
          value: 44
        },
        {
          name: 'East MCD',
          value: 15
        }

      ]
    },
    {
      name: 'RTP',
      series: [
        {
          name: 'North MCD',
          value: 14
        },
        {
          name: 'South MCD',
          value: 25
        },
        {
          name: 'East MCD',
          value: 20
        }

      ]
    },
    {
      name: 'COW',
      series: [
        {
          name: 'North MCD',
          value: 3
        },
        {
          name: 'South MCD',
          value: 5
        },
        {
          name: 'East MCD',
          value: 0
        }

      ]
    },


  ];

  towerType: String = 'GBM';
  mcd: String = 'North MCD';
  rendringData: any = [
    {
      "indus_id": 'IN-1243275',
      "tower_type": this.towerType,
      "address": 'A 32 Karol bagh',
      "mcd": "North MCD",
      "zone": 'Karol Bagh',
      "file_submission_date": '12/08/2021',
      "permission_status": "Pending"
    },
    {
      "indus_id": 'IN-1243276',
      "tower_type": this.towerType,
      "address": 'A 32 Civil Lines',
      "mcd": "North MCD",
      "zone": 'Civil Lines',
      "file_submission_date": '12/08/2021',
      "permission_status": "Pending"
    },
    {
      "indus_id": 'IN-1243277',
      "tower_type": this.towerType,
      "address": 'AB 22 Narela',
      "mcd": "North MCD",
      "zone": 'Narela',
      "file_submission_date": '12/08/2021',
      "permission_status": "Pending"
    },
    {
      "indus_id": 'IN-1243278',
      "tower_type": this.towerType,
      "address": 'C 42 Civil Lines',
      "mcd": "North MCD",
      "zone": 'Civil Lines',
      "file_submission_date": '12/08/2021',
      "permission_status": "Pending"
    },
    {
      "indus_id": 'IN-1243279',
      "tower_type": this.towerType,
      "address": '15/2 Rohini',
      "mcd": "North MCD",
      "zone": 'Rohini',
      "file_submission_date": '12/08/2021',
      "permission_status": "Pending"
    },
    {
      "indus_id": 'IN-1243280',
      "tower_type": this.towerType,
      "address": 'A 392 Karol bagh',
      "mcd": "North MCD",
      "zone": 'Karol Bagh',
      "file_submission_date": '12/08/2021',
      "permission_status": "Pending"
    },
    {
      "indus_id": 'IN-1243281',
      "tower_type": this.towerType,
      "address": 'D 48/33 Karol bagh',
      "mcd": "North MCD",
      "zone": 'Karol Bagh',
      "file_submission_date": '12/08/2021',
      "permission_status": "Pending"
    },
  ];

  onSelect(modalData: any, data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    this.towerType = data.series;
    this.mcd = data.name;
    console.log(this.towerType);
    this.triggerModal(modalData);
  }


  triggerModal(content: any) {
    console.log(content);
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title', windowClass: "myCustomModalClass" }).result.then((res: any) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res: any) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  overViewData1: any = [
    {
      "name": "OCT 2021",
      "series": [
        {
          "name": "GBM",
          "value": 12
        },
        {
          "name": "RTP",
          "value": 5
        },
        {
          "name": "COW",
          "value": 8
        }
      ]
    },

    {
      "name": "Nov 2021",
      "series": [
        {
          "name": "GBM",
          "value": 12
        },
        {
          "name": "RTP",
          "value": 15
        },
        {
          "name": "COW",
          "value": 16
        }
      ]
    },

    {
      "name": "Dec 2021",
      "series": [
        {
          "name": "GBM",
          "value": 2
        },
        {
          "name": "RTP",
          "value": 3
        },
        {
          "name": "COW",
          "value": 4
        }
      ]
    }

  ];

  overViewData2: any = [
    {
      "name": "Oct 2021",
      "series": [
        {
          "name": "GBM",
          "value": 6
        },
        {
          "name": "RTP",
          "value": 14
        },
        {
          "name": "COW",
          "value": 10
        }
      ]
    },

    {
      "name": "Nov 2021",
      "series": [
        {
          "name": "GBM",
          "value": 17
        },
        {
          "name": "RTP",
          "value": 11
        },
        {
          "name": "COW",
          "value": 5
        }
      ]
    },

    {
      "name": "Dec 2021",
      "series": [
        {
          "name": "GBM",
          "value": 5
        },
        {
          "name": "RTP",
          "value": 4
        },
        {
          "name": "COW",
          "value": 7
        }
      ]
    }

  ];

  overViewData3: any = [
    {
      "name": "Oct 2021",
      "series": [
        {
          "name": "GBM",
          "value": 9
        },
        {
          "name": "RTP",
          "value": 4
        },
        {
          "name": "COW",
          "value": 12
        }
      ]
    },

    {
      "name": "Nov 2021",
      "series": [
        {
          "name": "GBM",
          "value": 9
        },
        {
          "name": "RTP",
          "value": 8
        },
        {
          "name": "COW",
          "value": 2
        }
      ]
    },

    {
      "name": "Dec 2021",
      "series": [
        {
          "name": "GBM",
          "value": 6
        },
        {
          "name": "RTP",
          "value": 3
        },
        {
          "name": "COW",
          "value": 2
        }
      ]
    }

  ];

  yAxisLabel1 = 'Time Period';
  xAxisLabel1 = 'Permissions';

  colorScheme3: any = {
    domain: ['#5AA454', '#A10A28', '#C7B42C']
  };


  overViewData4: any = [
    {
      "name": "OCT 2021",
      "series": [
        {
          "name": "GBM",
          "value": 442
        },
        {
          "name": "RTP",
          "value": 285
        },
        // {
        //   "name": "COW",
        //   "value": 8
        // }
      ]
    },

    {
      "name": "Nov 2021",
      "series": [
        {
          "name": "GBM",
          "value": 592
        },
        {
          "name": "RTP",
          "value": 496
        },
        // {
        //   "name": "COW",
        //   "value": 16
        // }
      ]
    },

    {
      "name": "Dec 2021",
      "series": [
        {
          "name": "GBM",
          "value": 332
        },
        {
          "name": "RTP",
          "value": 195
        },
        // {
        //   "name": "1990",
        //   "value": 4
        // }
      ]
    }

  ];


}

