import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoSouthMcdComponent } from './reco-south-mcd.component';

describe('RecoSouthMcdComponent', () => {
  let component: RecoSouthMcdComponent;
  let fixture: ComponentFixture<RecoSouthMcdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecoSouthMcdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoSouthMcdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
