import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reco-south-mcd',
  templateUrl: './reco-south-mcd.component.html',
  styleUrls: ['./reco-south-mcd.component.css']
})
export class RecoSouthMcdComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  view: any[] = [700, 400];

  // options
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = true;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Region';
  showYAxisLabel: boolean = true;
  yAxisLabel: string = 'Sites';
  legendTitle: string = 'Years';

  colorScheme: any = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA', '#CB3B1D', '#23D8E3', '#BA23E3']
  };

  onSelect(data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  multi: any = [
    {
      name: 'Green Park',
      series: [
        {
          name: 'Total Sites',
          value: 592
        },
        {
          name: 'Dismantled',
          value: 77
        },
        {
          name: 'Old Permission',
          value: 174
        },
        {
          name: 'Fresh Permissions',
          value: 314
        },
        // {
        //   name: 'Old & Fresh Permission',
        //   value: 4
        // },
        
        {
          name: 'Total Permission Pending',
          value: 20
        },
        {
          name: 'Pole Sites Before 2016',
          value: 17
        },
        // {
        //   name: 'Net Permssion Pending',
        //   value: 40
        // }
      ]
    },
    {
      name: 'Lajpat Nagar',
      series: [
        {
          name: 'Total Sites',
          value: 530
        },
        {
          name: 'Dismantled',
          value: 52
        },
        {
          name: 'Old Permission',
          value: 182
        },
        {
          name: 'Fresh Permissions',
          value: 246
        },
        // {
        //   name: 'Old & Fresh Permission',
        //   value: 3
        // },
        // {
        //   name: 'Permission Denied',
        //   value: 0
        // },
        {
          name: 'Total Permission Pending',
          value: 46
        },
        {
          name: 'Pole Sites Before 2016',
          value: 26
        },
        // {
        //   name: 'Net Permssion Pending',
        //   value: 40
        // }
      ]
    },
    {
      name: 'Najafgarh',
      series: [
        {
          name: 'Total Sites',
          value: 368
        },
        {
          name: 'Dismantled',
          value: 11
        },
        {
          name: 'Old Permission',
          value: 262
        },
        {
          name: 'Fresh Permissions',
          value: 87
        },
        // {
        //   name: 'Old & Fresh Permission',
        //   value: 4
        // },
        // {
        //   name: 'Permission Denied',
        //   value: 1
        // },
        {
          name: 'Total Permission Pending',
          value: 8
        },
        {
          name: 'Pole Sites Before 2016',
          value: 7
        },
        // {
        //   name: 'Net Permssion Pending',
        //   value: 4
        // }
      ]
    },
    {
      name: 'Rajouri Garden',
      series: [
        {
          name: 'Total Sites',
          value: 453
        },
        {
          name: 'Dismantled',
          value: 45
        },
        {
          name: 'Old Permission',
          value: 204
        },
        {
          name: 'Fresh Permissions',
          value: 199
        },
        // {
        //   name: 'Old & Fresh Permission',
        //   value: 11
        // },
        // {
        //   name: 'Permission Denied',
        //   value: 0
        // },
        {
          name: 'Total Permission Pending',
          value: 5
        },
        {
          name: 'Pole Sites Before 2016',
          value: 2
        },
        // {
        //   name: 'Net Permssion Pending',
        //   value: 3
        // }
      ]
    }
  ];


  //Grahp 3 and 4

  overViewData: any[] = [
    {
      "name": "Green Park",
      "value": 38520000
    },
    {
      "name": "Lajpat Nagar",
      "value": 32320000
    },
    {
      "name": "Najafgarh",
      "value": 14150000
    },
    {
      "name": "Rajouri Garden",
      "value": 46700000
    }
  ];


  // options
  // showXAxis = true;
  // showYAxis = true;
  // gradient = false;
  // showLegend = true;
  // showXAxisLabel = true;
  xAxisLabel1 = 'Cities/Area';
  // showYAxisLabel = true;
  yAxisLabel1 = 'Fund';

  colorScheme2: any = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };


  financialData: any[] = [
    {
      "name": "Amount FD",
      "value": 127200000
    },
    {
      "name": "Amount DD",
      "value": 127200000
    },
    {
      "name": "Amount Agt. Intrest",
      "value": 101016817
    },
    {
      "name": "Aount Utilized By Zone",
      "value": 149930000
    },
    {
      "name": "Balance Available",
      "value": 208376817
    },
    {
      "name": "Amount Required",
      "value": 15920000
    }
  ];

  xAxisLabel2 = 'Medium';
  yAxisLabel2 = 'Amount';



}
