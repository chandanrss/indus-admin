import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-reco-north-mcd',
  templateUrl: './reco-north-mcd.component.html',
  styleUrls: ['./reco-north-mcd.component.css']
})
export class RecoNorthMcdComponent implements OnInit {

  closeModal!: string;

  constructor(private httpClient: HttpClient, private modalService: NgbModal) { }

  ngOnInit(): void {
    // this.getSiteList();
  }

  view: any[] = [700, 400];

  // options
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = true;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Region';
  showYAxisLabel: boolean = true;
  yAxisLabel: string = 'Sites';
  legendTitle: string = 'Summary';

  colorScheme: any = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA', '#CB3B1D', '#23D8E3', '#BA23E3']
  };

  rendringData: any = [
    {
      "sno": 450,
      "indus_id": 'IN-1045124',
      "tower_type": 'Tower Site',
      "address": '30, Gandhi Market, Near Rajnit Hotel, N.D.',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    },
    {
      "sno": 451,
      "indus_id": 'IN-1045165',
      "tower_type": 'Tower Site',
      "address": '583, Gauri Shanker Mkt., Katra Neel',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 452,
      "indus_id": 'IN-1118965',
      "tower_type": 'Tower Site',
      "address": '4614-4616, Main Road ward NO XIV Pahari Dhiraj Delhi. 110006',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 450,
      "indus_id": 'IN-1044938',
      "tower_type": 'Pole Site',
      "address": 'house no. 2294, Gali Qasimjan, Ballimaran Delhi-110006',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 450,
      "indus_id": 'IN-1045214',
      "tower_type": 'Tower Site',
      "address": '28, Hathi Kha, Azad Market. Delhi',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 450,
      "indus_id": 'IN-1045275',
      "tower_type": 'Tower Site',
      "address": '2924-2929, Ward no. XIII, Main Bahadur Garh Road New Delhi.',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    },
  ];
  rendringData2: any = [
    {
      "sno": 625,
      "indus_id": 'IN-1010648',
      "tower_type": 'Tower Site',
      "address": '15/10830 Nabi Karim Jhandewalan Road, Paharganj, new delhi-55',
      "date_of_installation": "2/02/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    },
    {
      "sno": 641,
      "indus_id": 'IN-1119751',
      "tower_type": 'Tower Site',
      "address": '5568, Nai Sarak, FF, Ward No.6, Delhi-06',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 672,
      "indus_id": 'IN-1008389',
      "tower_type": 'Tower Site',
      "address": '6/1,48, Racket Road, Rajpur Road, New Delhi',
      "date_of_installation": "15/05/2003",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 679,
      "indus_id": 'IN-1249791',
      "tower_type": 'Pole Site',
      "address": 'Shop no-643, Old Lajpat Rai Market, Delhi-110006 Shop no-643, Old Lajpat Rai Market, Delhi-110006',
      "date_of_installation": "08/06/2010",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 701,
      "indus_id": 'IN-1044712',
      "tower_type": 'Tower Site',
      "address": 'Khanna Market',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 706,
      "indus_id": 'IN-1045018',
      "tower_type": 'Tower Site',
      "address": '2924-2929, Ward no. XIII, Main Bahadur Garh Road New Delhi.',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    },
  ];

  rendringData3: any = [
    {
      "sno": 450,
      "indus_id": 'IN-1045124',
      "tower_type": 'Tower Site',
      "address": '30, Gandhi Market, Near Rajnit Hotel, N.D.',
      "date_of_installation": "2/02/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    },
    {
      "sno": 451,
      "indus_id": 'IN-1045165',
      "tower_type": 'Tower Site',
      "address": '583, Gauri Shanker Mkt., Katra Neel',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 452,
      "indus_id": 'IN-1118965',
      "tower_type": 'Tower Site',
      "address": '4614-4616, Main Road ward NO XIV Pahari Dhiraj Delhi. 110006',
      "date_of_installation": "15/05/2003",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 453,
      "indus_id": 'IN-1044938',
      "tower_type": 'Pole Site',
      "address": 'house no. 2294, Gali Qasimjan, Ballimaran Delhi-110006',
      "date_of_installation": "08/06/2010",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 454,
      "indus_id": 'IN-1045214',
      "tower_type": 'Tower Site',
      "address": '28, Hathi Kha, Azad Market. Delhi',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    }, {
      "sno": 455,
      "indus_id": 'IN-1045275',
      "tower_type": 'Tower Site',
      "address": '2924-2929, Ward no. XIII, Main Bahadur Garh Road New Delhi.',
      "date_of_installation": "1/12/2007",
      "permission_type": 'Old Permission',
      "amount_adjust": '0.00',
    },
  ];

  isDismantled = false;
  isOldPermission = false;
  onSelect(modalData: any, data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    if (data.name == "Dismantled") {
      this.rendringData = this.rendringData2;
      this.isDismantled = true;
    } else if (data.name == 'Old Permission') {
      this.rendringData = this.rendringData3;
      this.isOldPermission = true;
      this.isDismantled = false;
    }
    this.triggerModal(modalData);
  }


  triggerModal(content: any) {
    console.log(content);
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title', windowClass: "myCustomModalClass" }).result.then((res: any) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res: any) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  multi: any = [
    {
      name: 'City SP',
      series: [
        {
          name: 'Total Sites',
          value: 226
        },
        {
          name: 'Dismantled',
          value: 7
        },
        {
          name: 'Old Permission',
          value: 185
        },
        {
          name: 'Fresh Permissions',
          value: 73
        },
        {
          name: 'Old & Fresh Permission',
          value: 0
        },
        {
          name: 'Permission Denied',
          value: 0
        },
        {
          name: 'Total Permission Pending',
          value: 1
        },
        {
          name: 'Pole Sites Before 2016',
          value: 0
        },
        {
          name: 'Net Permssion Pending',
          value: 1
        }
      ]
    },
    {
      name: 'Civil Lines',
      series: [
        {
          name: 'Total Sites',
          value: 226
        },
        {
          name: 'Dismantled',
          value: 5
        },
        {
          name: 'Old Permission',
          value: 87
        },
        {
          name: 'Fresh Permissions',
          value: 75
        },
        {
          name: 'Old & Fresh Permission',
          value: 2
        },
        {
          name: 'Permission Denied',
          value: 0
        },
        {
          name: 'Total Permission Pending',
          value: 57
        },
        {
          name: 'Pole Sites Before 2016',
          value: 32
        },
        {
          name: 'Net Permssion Pending',
          value: 25
        }
      ]
    },
    {
      name: 'Karol Bagh',
      series: [
        {
          name: 'Total Sites',
          value: 344
        },
        {
          name: 'Dismantled',
          value: 19
        },
        {
          name: 'Old Permission',
          value: 208
        },
        {
          name: 'Fresh Permissions',
          value: 61
        },
        {
          name: 'Old & Fresh Permission',
          value: 0
        },
        {
          name: 'Permission Denied',
          value: 0
        },
        {
          name: 'Total Permission Pending',
          value: 57
        },
        {
          name: 'Pole Sites Before 2016',
          value: 32
        },
        {
          name: 'Net Permssion Pending',
          value: 25
        }
      ]
    }
    
  ];

  multi2: any = [
    {
      name: 'Keshavpuram',
      series: [
        {
          name: 'Total Sites',
          value: 288
        },
        {
          name: 'Dismantled',
          value: 34
        },
        {
          name: 'Old Permission',
          value: 125
        },
        {
          name: 'Fresh Permissions',
          value: 100
        },
        {
          name: 'Old & Fresh Permission',
          value: 1
        },
        {
          name: 'Permission Denied',
          value: 0
        },
        {
          name: 'Total Permission Pending',
          value: 28
        },
        {
          name: 'Pole Sites Before 2016',
          value: 13
        },
        {
          name: 'Net Permssion Pending',
          value: 15
        }
      ]
    },
    {
      name: 'Narela',
      series: [
        {
          name: 'Total Sites',
          value: 250
        },
        {
          name: 'Dismantled',
          value: 7
        },
        {
          name: 'Old Permission',
          value: 119
        },
        {
          name: 'Fresh Permissions',
          value: 57
        },
        {
          name: 'Old & Fresh Permission',
          value: 0
        },
        {
          name: 'Permission Denied',
          value: 0
        },
        {
          name: 'Total Permission Pending',
          value: 67
        },
        {
          name: 'Pole Sites Before 2016',
          value: 2
        },
        {
          name: 'Net Permssion Pending',
          value: 65
        }
      ]
    },
    {
      name: 'Rohini',
      series: [
        {
          name: 'Total Sites',
          value: 310
        },
        {
          name: 'Dismantled',
          value: 22
        },
        {
          name: 'Old Permission',
          value: 157
        },
        {
          name: 'Fresh Permissions',
          value: 66
        },
        {
          name: 'Old & Fresh Permission',
          value: 0
        },
        {
          name: 'Permission Denied',
          value: 0
        },
        {
          name: 'Total Permission Pending',
          value: 65
        },
        {
          name: 'Pole Sites Before 2016',
          value: 23
        },
        {
          name: 'Net Permssion Pending',
          value: 42
        }
      ]
    }
  ];

  permissionData1: any[] = [
    {
      name: 'Total Sites',
      value: 226
    },
    {
      name: 'Dismantled',
      value: 7
    },
    {
      name: 'Old Permission',
      value: 185
    },
    {
      name: 'Fresh Permissions',
      value: 73
    },
    
    {
      name: 'Permission Pending',
      value: 1
    }
    
  ];




  //Grahp 3 and 4

  overViewData: any[] = [
    {
      "name": "City SP",
      "value": 8070000
    },
    {
      "name": "Civil Lines",
      "value": 18290000
    },
    {
      "name": "Karol Bagh",
      "value": 16640000
    },
    {
      "name": "Keshavpuram",
      "value": 21610000
    },
    {
      "name": "Narela",
      "value": 5050000
    },
    {
      "name": "Rohini",
      "value": 16630000
    }
  ];


  // options
  // showXAxis = true;
  // showYAxis = true;
  // gradient = false;
  // showLegend = true;
  // showXAxisLabel = true;
  xAxisLabel1 = 'Cities/Area';
  // showYAxisLabel = true;
  yAxisLabel1 = 'Fund';

  colorScheme2: any = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };


  financialData: any[] = [
    {
      "name": "Amount FD",
      "value": 51250000
    },
    {
      "name": "Amount DD",
      "value": 51250000
    },
    {
      "name": "Amount Agt. Intrest",
      "value": 40700565
    },
    {
      "name": "Aount Utilized By Zone",
      "value": 86290000
    },
    {
      "name": "Balance Available",
      "value": 56910563
    }
  ];

  xAxisLabel2 = 'Medium';
  yAxisLabel2 = 'Amount';

  // tempData: any;

  // getSiteList() {
  //   let route = '../../../assets/img/indus_summary.csv';

  //   this.httpClient.get(route, { responseType: 'text' })
  //     .subscribe((res: any) => {
  //       console.log(res);
  //       const list = res.split("\n");
  //       list.forEach((e: any) => {

  //         this.tempData.push(e);
  //       });
  //       console.log(this.tempData);
  //     });
  // }

}
