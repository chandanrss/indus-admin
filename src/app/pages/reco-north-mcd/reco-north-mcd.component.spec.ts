import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoNorthMcdComponent } from './reco-north-mcd.component';

describe('RecoNorthMcdComponent', () => {
  let component: RecoNorthMcdComponent;
  let fixture: ComponentFixture<RecoNorthMcdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecoNorthMcdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoNorthMcdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
