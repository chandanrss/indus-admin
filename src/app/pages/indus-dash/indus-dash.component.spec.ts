import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndusDashComponent } from './indus-dash.component';

describe('IndusDashComponent', () => {
  let component: IndusDashComponent;
  let fixture: ComponentFixture<IndusDashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndusDashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndusDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
