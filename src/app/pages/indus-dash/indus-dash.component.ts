import { Component, OnInit } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@Component({
  selector: 'app-indus-dash',
  templateUrl: './indus-dash.component.html',
  styleUrls: ['./indus-dash.component.css']
})
export class IndusDashComponent implements OnInit {

  constructor() { }

  single: any[] = [
    {
      "name": "Old Permission ",
      "value": 8940
    },
    {
      "name": "Dismantled",
      "value": 5000
    },
    {
      "name": "Fresh Permission ",
      "value": 7200
    },
    {
      "name": "Permission Pending",
      "value": 6200
    }
  ];

  view: any[] = [500, 400];

  // options
  showLegend: boolean = true;
  showLabels: boolean = true;

  colorScheme: any = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  ngOnInit(): void {
  }

  onSelect(event: any) {
    console.log(event);
  }

}
