import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IndusDashComponent } from './indus-dash/indus-dash.component';
import { VendorDashComponent } from './vendor-dash/vendor-dash.component';
import { AddRecoComponent } from './add-reco/add-reco.component';
import { AddBuiltComponent } from './add-built/add-built.component';
import { SitesComponent } from './sites/sites.component';
import { SitesPermissionComponent } from './sites-permission/sites-permission.component';
import { SitesPayForcastComponent } from './sites-pay-forcast/sites-pay-forcast.component';
import { RecoDashComponent } from './reco-dash/reco-dash.component';
import { RecoNorthMcdComponent } from './reco-north-mcd/reco-north-mcd.component';
import { RecoSouthMcdComponent } from './reco-south-mcd/reco-south-mcd.component';
import { RecoEastMcdComponent } from './reco-east-mcd/reco-east-mcd.component';
import { NewBuildDashComponent } from './new-build-dash/new-build-dash.component';
import { RecoListComponent } from './reco-list/reco-list.component';
import { BuiltListComponent } from './built-list/built-list.component';

const routes: Routes = [
  {
    path: '',
    component: IndusDashComponent,
  },
  {
    path: 'dash',
    component: DashboardComponent,
  },
  {
    path: 'indus-dash',
    component: IndusDashComponent
  },

  {
    path: 'vendor-dash',
    component: VendorDashComponent
  },
  {
    path: 'reco-add',
    component: AddRecoComponent
  },
  {
    path: 'reco-list',
    component: RecoListComponent
  },
  {
    path: 'built-list',
    component: BuiltListComponent
  },
  {
    path: 'built-add',
    component: AddBuiltComponent
  },
  {
    path: 'site',
    component: SitesComponent
  },
  {
    path: 'site/:filterParam',
    component: SitesComponent
  },
  {
    path: 'site-permission',
    component: SitesPermissionComponent
  },
  {
    path: 'site-pay',
    component: SitesPayForcastComponent
  },
  {
    path: 'reco-dash',
    component: RecoDashComponent
  },
  {
    path: 'reco-north-mcd',
    component: RecoNorthMcdComponent
  },
  {
    path: 'reco-south-mcd',
    component: RecoSouthMcdComponent
  },
  {
    path: 'reco-east-mcd',
    component: RecoEastMcdComponent
  },


  {
    path: 'new-build-dash',
    component: NewBuildDashComponent
  }

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class PagesRoutingModule { }
