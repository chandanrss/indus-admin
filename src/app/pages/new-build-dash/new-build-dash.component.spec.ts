import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBuildDashComponent } from './new-build-dash.component';

describe('NewBuildDashComponent', () => {
  let component: NewBuildDashComponent;
  let fixture: ComponentFixture<NewBuildDashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewBuildDashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBuildDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
