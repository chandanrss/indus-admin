import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-build-dash',
  templateUrl: './new-build-dash.component.html',
  styleUrls: ['./new-build-dash.component.css']
})
export class NewBuildDashComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {
  }

  view: any[] = [700, 400];

  // options
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = true;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Zone';
  showYAxisLabel: boolean = true;
  yAxisLabel: string = 'Permission (In Numbers)';
  legendTitle: string = 'Permission Param';
  showDataLabel: boolean = true;
  colorScheme: any = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA', '#CB3B1D', '#23D8E3', '#BA23E3']
  };

  onSelect(data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  multi: any = [
    {
      name: 'North MCD',
      series: [
        {
          name: 'Files Perpared',
          value: 92
        },
        {
          name: 'Files handed over for submission',
          value: 92
        },
        {
          name: 'Acknowledgement Received',
          value: 80
        },
        {
          name: 'Acknowledgement Pending',
          value: 12
        },
        {
          name: 'Permission Recieved',
          value: 60
        },
        {
          name: 'Permission Pending',
          value: 20
        }
      ]
    },
    {
      name: 'East MCD',
      series: [
        {
          name: 'Files Perpared',
          value: 5
        },
        {
          name: 'Files handed over for submission',
          value: 5
        },
        {
          name: 'Acknowledgement Received',
          value: 5
        },
        {
          name: 'Acknowledgement Pending',
          value: 0
        },
        {
          name: 'Permission Recieved',
          value: 5
        },
        {
          name: 'Permission Pending',
          value: 0
        }
      ]
    },
    {
      name: 'South MCD',
      series: [
        {
          name: 'Files Perpared',
          value: 115
        },
        {
          name: 'Files handed over for submission',
          value: 115
        },
        {
          name: 'Acknowledgement Received',
          value: 100
        },
        {
          name: 'Acknowledgement Pending',
          value: 15
        },
        {
          name: 'Permission Recieved',
          value: 20
        },
        {
          name: 'Permission Pending',
          value: 80
        }
      ]
    },
  ];


  //Grahp 3 and 4

  overViewData: any[] = [
    {
      "name": "Green Park",
      "value": 12
    },
    {
      "name": "Lajpat Nagar",
      "value": 18
    },
    {
      "name": "Najafgarh",
      "value": 36
    }
  ];


  // options
  // showXAxis = true;
  // showYAxis = true;
  // gradient = false;
  // showLegend = true;
  // showXAxisLabel = true;
  xAxisLabel1 = 'Cities/Area';
  // showYAxisLabel = true;
  yAxisLabel1 = 'Days';

  colorScheme2: any = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };


  financialData: any[] = [
    {
      "name": "Green Park",
      "value": 30
    },
    {
      "name": "Lajpat Nagar",
      "value": 5
    },
    {
      "name": "Najafgarh",
      "value": 13
    }
  ];

  xAxisLabel2 = 'Permission Pending Summary';
  yAxisLabel2 = 'No of Permission';



  // Pie Chart

  single2: any = [
    {
      "name": "Files Handed Over for Submission",
      "value": 200
    },
    {
      "name": "Acknowledgement Received",
      "value": 180
    },
    {
      "name": "Acknowledgement Pending",
      "value": 20
    },
    {
      "name": "Permissions Recieved",
      "value": 100
    },
    {
      "name": "Permission Pending",
      "value": 80
    }
  ];
  view2: Number[] = [400, 233]

  // options
  // gradient: boolean = true;
  showLegend2: boolean = true;
  showLabels2: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: any = 'below';

  colorScheme3: any = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };


  single3: any = [
    {
      "name": "Files Handed Over for Submission",
      "value": 12
    },
    {
      "name": "Acknowledgement Received",
      "value": 12
    },
    {
      "name": "Acknowledgement Pending",
      "value": 0
    },
    {
      "name": "Permissions Recieved",
      "value": 10
    },
    {
      "name": "Permission Pending",
      "value": 2
    }
  ];

  // options
  // gradient: boolean = true;
  showLegend3: boolean = true;
  showLabels3: boolean = true;
  isDoughnut1: boolean = false;
  legendPosition1: any = 'below';

  colorScheme4: any = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA', '#CB3B1D']
  };



  //horizontal graph
  permissionRecivedSummary: any = [
    {
      "name": "Civil Lines",
      "value": 50
    },
    {
      "name": "Rohini",
      "value": 10
    },
    {
      "name": "Green Park",
      "value": 5
    },
    {
      "name": "Narela",
      "value": 15
    }, {
      "name": "Lajpat Nagar",
      "value": 18
    }
  ];

  showXAxis5: boolean = true;
  showYAxis5: boolean = true;
  gradient5: boolean = false;
  showLegend5: boolean = false;
  showXAxisLabel5: boolean = true;
  yAxisLabel5: string = 'Zone';
  showYAxisLabel5: boolean = true;
  xAxisLabel5: string = 'Permissions Recieved';



  permissionRecivedSummary2: any = [
    {
      "name": "Shahdara North",
      "value": 5
    },
    {
      "name": "Najafgarh",
      "value": 3
    },
    {
      "name": "Rajouri Garden",
      "value": 15
    }
  ];
}