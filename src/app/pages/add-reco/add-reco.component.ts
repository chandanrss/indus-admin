import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-reco',
  templateUrl: './add-reco.component.html',
  styleUrls: ['./add-reco.component.css']
})
export class AddRecoComponent implements OnInit {

  RecoForm!: FormGroup;
  config: any;
  submitted = false;
  unitId: any = '';
  unitMaster = [];
  IngredientFormId: any;
  unitList: any;
  ingredient: any;
  filterKey: any = ['search_text'];
  filterParam: any = {
    search_text: '',
    itemsPerPage: 1,
    currentPage: 1,
    totalItems: 1,
    from: 1,
    to: 10,
  };
  filterString: any = '?';

  updateButton: any;

  constructor(
    private fb: FormBuilder, public router: Router, private _routeURL: ActivatedRoute) {
  }

  ngOnInit() {

    this.RecoFormInit();

    if (this.unitList && this.unitId) {
      this.RecoForm.patchValue(this.unitList);
    }
  }

  RecoFormInit() {
    this.RecoForm = this.fb.group({
      Site_Id: ['0', Validators.required],
      Zone: ['0', Validators.required],
      estimated_date: ['', Validators.required],
      Fee_payable: ['', Validators.required],
      submission_ack: ['', Validators.required],
      pending_reminder: ['', Validators.required],
      lop: ['', Validators.required],
      property_papers: ['', Validators.required],
      rent_agree: ['', Validators.required],
      owner_noc: ['', Validators.required],
      permission_img: ['', Validators.required],
      safc: ['', Validators.required],
      site_photo: ['', Validators.required],
      lop_date: ['', Validators.required],
      property_papers_date: ['', Validators.required],
      rent_agree_date: ['', Validators.required],
      owner_noc_date: ['', Validators.required],
      permission_img_date: ['', Validators.required],
      safc_date: ['', Validators.required],
      site_photo_date: ['', Validators.required],
    });
  }


  get RecoFormControl() {
    return this.RecoForm.controls;
  }
  // submit form
  onSubmitRecoForm() {
    this.submitted = true;
    if (this.RecoForm.invalid) {

      return;
    }
    console.log(this.RecoForm.value);
  }

  generalFormSetting() {
    this.submitted = false;

    this.RecoFormInit();
    this.router.navigateByUrl('/sudo/master/unit/list');
  }

}