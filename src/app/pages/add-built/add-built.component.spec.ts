import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBuiltComponent } from './add-built.component';

describe('AddBuiltComponent', () => {
  let component: AddBuiltComponent;
  let fixture: ComponentFixture<AddBuiltComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddBuiltComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBuiltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
