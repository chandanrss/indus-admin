import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-add-built',
  templateUrl: './add-built.component.html',
  styleUrls: ['./add-built.component.css']
})
export class AddBuiltComponent implements OnInit {

  NewBuildForm!: FormGroup;
  config: any;
  submitted = false;
  unitId: any = '';
  unitMaster = [];
  IngredientFormId: any;
  unitList: any;
  ingredient: any;
  filterKey: any = ['search_text'];
  filterParam: any = {
    search_text: '',
    itemsPerPage: 1,
    currentPage: 1,
    totalItems: 1,
    from: 1,
    to: 10,
  };
  filterString: any = '?';

  updateButton: any;

  constructor(
    private fb: FormBuilder, public router: Router, private _routeURL: ActivatedRoute, private _toastr: ToastrService) {
  }

  ngOnInit() {
    // this.showSuccess();
    this.NewBuildFormInit();

    if (this.unitList && this.unitId) {
      this.NewBuildForm.patchValue(this.unitList);
    }
  }

  showSuccess() {
    this._toastr.success('Success', 'Form Data Saved Successfully!');
  }

  NewBuildFormInit() {
    this.NewBuildForm = this.fb.group({
      Site_Id: ['0', Validators.required],
      Zone: ['0', Validators.required],
      lop: [''],
      property_papers: [''],
      rent_agree: [''],
      owner_noc: [''],
      property_papers_date: [''],
      rent_agree_date: [''],
      owner_noc_date: [''],
      lop_date: [''],
    });
  }


  get NewBuildFormControl() {
    return this.NewBuildForm.controls;
  }
  // submit form
  onSubmitNewBuildForm() {
    this.submitted = true;
    if (this.NewBuildForm.invalid) {
      return;
    }
    this.showSuccess();
    console.log(this.NewBuildForm.value);
    this.router.navigateByUrl('/home/built-list');


  }
  generalFormSetting() {
    this.submitted = false;
    this.NewBuildFormInit();
    this.router.navigateByUrl('/home/built-add');
  }

}