import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuiltListComponent } from './built-list.component';

describe('BuiltListComponent', () => {
  let component: BuiltListComponent;
  let fixture: ComponentFixture<BuiltListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuiltListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuiltListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
