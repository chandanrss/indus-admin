import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor } from '@angular/common/http';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.css']
})
export class SitesComponent implements OnInit {
  filterParam: any;
  constructor(private httpClient: HttpClient, private spinner: NgxSpinnerService, private route: ActivatedRoute) {
    this.filterParam = this.route.snapshot.paramMap.get('filterParam');
    console.log(this.filterParam);

  }
  siteList: any;
  dtOptions: DataTables.Settings = {};

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();

  ngOnInit(): void {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 15
    };
    this.getSiteList();




    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
  }

  tempData: any = [];

  getSiteList() {
    let route = '../../../assets/img/indus_summary_2.tsv';

    this.httpClient.get(route, { responseType: 'text' })
      .subscribe((res: any) => {
        // console.log(res);
        const list = res.split("\n");
        list.forEach((e: any) => {
          this.tempData.push(e.split("\t"));
        });
        // console.log(this.tempData);
        this.siteList = (this.tempData as any);

        if (this.filterParam == 'Dismantled' || this.filterParam == 'Permission Pending' || this.filterParam == 'Fresh Permission' || this.filterParam == 'Old Permission') {
          this.siteList = this.applyFilter(this.filterParam);
          this.dtTrigger.next();
        } else
          this.dtTrigger.next();

        console.log(this.siteList);
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
      });
  }

  applyFilter(param: any = null) {
    let tempVar: any = [];
    this.siteList.forEach((element: any) => {
      if (element[14])
        if (element[14].toString().trim() == param.toString().trim()) {
          tempVar.push(element);
        }
    });
    return tempVar;

  }

}
