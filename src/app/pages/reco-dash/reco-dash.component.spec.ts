import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoDashComponent } from './reco-dash.component';

describe('RecoDashComponent', () => {
  let component: RecoDashComponent;
  let fixture: ComponentFixture<RecoDashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecoDashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
