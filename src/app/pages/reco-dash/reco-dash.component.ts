import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reco-dash',
  templateUrl: './reco-dash.component.html',
  styleUrls: ['./reco-dash.component.css']
})
export class RecoDashComponent implements OnInit {
  isNorthMCDTable: any = true;
  isSouthMCDTable: any = true;
  isEastMCDTable: any = true;
  constructor(private httpClient: HttpClient, private router: Router) { }

  ngOnInit(): void {
    // this.getSiteList();
  }

  overViewData: any[] = [
    {
      "name": "Tot. Scope of Work",
      "value": 4443
    },
    {
      "name": "Permission Granted",
      "value": 3536
    },
    {
      "name": "Fresh Permission RECO",
      "value": 1376
    },
    {
      "name": "Permission Pending",
      "value": 557
    },

    {
      "name": "Not Required",
      "value": 350
    }


  ];

  overViewData1: any[] = [
    {
      "name": "Total Sites",
      "value": 4406
    },
    {
      "name": "Dismantled",
      "value": 309
    },
    {
      "name": "Old Permission",
      "value": 2266
    },
    {
      "name": "Fresh Permission",
      "value": 1218
    },
    {
      "name": "Permission Pending",
      "value": 584
    }
  ];

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = false;
  xAxisLabel1 = 'Sites';
  showYAxisLabel = true;
  yAxisLabel1 = 'Sites In Nos';

  colorScheme: any = {
    domain: ['#00ffe9', '#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };


  financialData: any[] = [
    {
      "name": "Amount FD",
      "value": 51250000
    },
    {
      "name": "Amount DD",
      "value": 51250000
    },
    {
      "name": "Amount Agt. Intrest on FD",
      "value": 40700565
    },
    {
      "name": "Utilized for Issuing Permission",
      "value": 86290000
    },
    {
      "name": "Balance Amount",
      "value": 56910565
    }
  ];

  financialData2: any[] = [
    {
      "name": "Amount FD",
      "value": 127200000
    },
    {
      "name": "Amount DD",
      "value": 127200000
    },
    {
      "name": "Amount Agt. Intrest on FD",
      "value": 101016817
    },
    {
      "name": "Utilized for Issuing Permission",
      "value": 149930000
    },
    {
      "name": "Balance Amount",
      "value": 220066817
    }
  ];

  financialData3: any[] = [
    {
      "name": "Amount FD",
      "value": 39625000
    },
    {
      "name": "Amount DD",
      "value": 39625000
    },
    {
      "name": "Amount Agt. Intrest on FD",
      "value": 31468486
    },
    {
      "name": "Utilized for Issuing Permission",
      "value": 7890000
    },
    {
      "name": "Balance Amount",
      "value": 102828486
    }
  ];

  xAxisLabel2 = 'Fund Medium';
  yAxisLabel2 = 'Fund';


  // options
  // gradient: boolean = true;
  // showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: any = 'below';

  colorScheme2 = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  northMCDData = [
    {
      "name": Math.round((94 / 1684) * 100) + "%" + " Dismantled",
      "value": 94
    },
    {
      "name": Math.round((881 / 1684) * 100) + "%" + " Old Permissions",
      "value": 881
    },
    {
      "name": Math.round((432 / 1684) * 100) + "%" + " Fresh Permissions",
      "value": 432
    },
    {
      "name": Math.round((274 / 1684) * 100) + "%" + " Permissions Pending",
      "value": 274
    }
  ];

  southMCDData = [
    {
      "name": Math.round((185 / 1684) * 100) + "%" + " Dismantled",
      "value": 185
    },
    {
      "name": Math.round((807 / 1684) * 100) + "%" + " Old Permissions",
      "value": 807
    },
    {
      "name": Math.round((846 / 1684) * 100) + "%" + " Fresh Permissions",
      "value": 846
    },
    {
      "name": Math.round((80 / 1684) * 100) + "%" + " Permissions Pending",
      "value": 80
    }
  ];

  eastMCDData = [
    {
      "name": Math.round((31 / 1684) * 100) + "%" + " Dismantled",
      "value": 31
    },
    {
      "name": Math.round((579 / 1684) * 100) + "%" + " Old Permissions",
      "value": 579
    },
    {
      "name": Math.round((978 / 1684) * 100) + "%" + " Fresh Permissions",
      "value": 978
    },
    {
      "name": Math.round((69 / 1684) * 100) + "%" + " Permissions Pending",
      "value": 69
    }
  ];

  OnSelectPieChart(event: any) {
    this.isNorthMCDTable = false;
    this.isSouthMCDTable = true;
    this.isEastMCDTable = true;
    // this.router.navigateByUrl('/home/reco-north-mcd');
  }

  OnSelectPieChart1(event: any) {
    this.isNorthMCDTable = true;
    this.isSouthMCDTable = false;
    this.isEastMCDTable = true;
    // this.router.navigateByUrl('/home/reco-south-mcd');
  }

  OnSelectPieChart2(event: any) {
    this.isNorthMCDTable = true;
    this.isSouthMCDTable = true;
    this.isEastMCDTable = false;
    // this.router.navigateByUrl('/home/reco-east-mcd');
  }


  // tempData: any;

  // getSiteList() {
  //   let route = '../../../assets/img/indus_summary.csv';

  //   this.httpClient.get(route, { responseType: 'text' })
  //     .subscribe((res: any) => {
  //       console.log(res);
  //       const list = res.split("\n");
  //       list.forEach((e: any) => {
  //         this.tempData.push(e);
  //       });
  //       console.log(this.tempData);
  //     });
  // }

}
